# QuijoteLuiKS

Retorna la información del certificado digital
## Prerequisist
* Java 8
* Gradle 4

## Main method in
org.kse.QuijoteLuiKS

### Instructions
1. In informacion() method change the variable:
PKCS12_RESOURCE = "Your certificate folder path"
2. Run in IntelliJ (Recommended)

## Compilar
```
gradle build
```
## Comando en terminal para añadir al repositorio Maven Local (Linux/Mac)
```
mvn install:install-file -Dfile=./build/libs/QuijoteLuiKS-1.3.jar -DgroupId=org.quijotelui -DartifactId=QuijoteLuiKS -Dversion=1.3 -Dpackaging=jar
```
## Comando en terminal para añadir al repositorio Maven Local (Windows)
```
cd .\build\libs\
mvn install:install-file "-Dfile=QuijoteLuiKS-1.3.jar" "-DgroupId=org.quijotelui" "-DartifactId=QuijoteLuiKS" "-Dversion=1.3" "-Dpackaging=jar"
```